from django.urls import path

from consuming_any_api.views import WebhookHandlerView

urlpatterns = [
    path('webhook/<str:api_type>/<str:secret_key>/', WebhookHandlerView.as_view(), name='webhook_handler'),
]
