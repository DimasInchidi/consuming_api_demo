from pprint import pprint

from celery import shared_task
from rest_framework.exceptions import ValidationError

from consuming_any_api.serializers import MergeRequestEventSerializer


@shared_task
def dispatch_handler(api_type, data):
    pprint(data)
    if api_type == 'gitlab':
        if data['object_kind'] == 'merge_request':
            serializer = MergeRequestEventSerializer(data=data)
        else:
            raise ValidationError({'object_kind': 'Invalid object kind received.'})
    else:
        raise ValidationError({'error': f'API type with code {api_type} not handled.'})

    if serializer.is_valid(raise_exception=True):
        serializer.handle()
