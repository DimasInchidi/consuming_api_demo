from django.apps import AppConfig


class ConsumingAnyAPIConfig(AppConfig):
    name = 'consuming_any_api'
