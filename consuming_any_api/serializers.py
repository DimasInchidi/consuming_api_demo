from django.conf import settings
from rest_framework import serializers
from slack import WebClient
from telegram import Bot


class UserSerializer(serializers.Serializer):
    name = serializers.CharField()


class MergeRequestObjectAttributesSerializer(serializers.Serializer):
    action = serializers.CharField(required=False)
    target_branch = serializers.CharField()
    description = serializers.CharField()


class MergeRequestEventSerializer(serializers.Serializer):
    object_kind = serializers.CharField()
    user = UserSerializer(many=False)
    object_attributes = MergeRequestObjectAttributesSerializer(many=False)

    def handle(self):
        """
        handles https://gitlab.com/help/user/project/integrations/webhooks#merge-request-events
        """
        merger_name = self.validated_data['user']['name']
        target_branch = self.validated_data['object_attributes']['target_branch']
        description = self.validated_data['object_attributes']['description']

        message = f"""{merger_name} just merge to {target_branch}:
        {description}
        """

        if 'action' in self.validated_data \
                and self.validated_data['object_attributes']['action'] == 'merged':
            telegram_bot = Bot(token=settings.TELEGRAM_BOT_TOKEN)
            slack_client = WebClient(token=settings.SLACK_BOT_TOKEN)

            telegram_bot.send_message(
                chat_id=settings.PRIVATE_TELEGRAM_CHANNEL_ID,
                text=message
            )
            slack_client.chat_postMessage(
                channel=settings.PRIVATE_SLACK_CHANNEL_ID,
                text=message,
            )
