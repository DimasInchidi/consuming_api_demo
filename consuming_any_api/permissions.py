from django.conf import settings
from rest_framework.permissions import BasePermission


class WebhookHandlerPermission(BasePermission):

    def has_permission(self, request, view):
        if view.kwargs['api_type'] == 'gitlab' \
                and settings.GITLAB_PATH_SECRET == view.kwargs['secret_key'] \
                and request.headers.get('X-Gitlab-Token', None) == settings.GITLAB_WEBHOOK_TOKEN:
            return True
        return False
