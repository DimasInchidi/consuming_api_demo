from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from consuming_any_api.permissions import WebhookHandlerPermission
from consuming_any_api.tasks import dispatch_handler


@method_decorator(csrf_exempt, name='dispatch')
class WebhookHandlerView(APIView):
    permission_classes = (WebhookHandlerPermission,)

    def post(self, request, *args, **kwargs):
        dispatch_handler.delay(self.kwargs['api_type'], request.data)
        return Response(status=status.HTTP_200_OK)
